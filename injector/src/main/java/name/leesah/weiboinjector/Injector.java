package name.leesah.weiboinjector;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.sso.AccessTokenKeeper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import name.leesah.purger.sinaweibo.monitor.report.ProgressReport;
import name.leesah.sinaweiboapi.WeiboRequest;

import static com.android.volley.Request.Method.POST;
import static com.android.volley.toolbox.Volley.newRequestQueue;
import static com.sina.weibo.sdk.auth.sso.AccessTokenKeeper.readAccessToken;
import static java.lang.String.format;
import static java.util.Collections.singletonMap;
import static java.util.Locale.US;
import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;
import static name.leesah.purger.sinaweibo.monitor.Constants.ACTION;
import static name.leesah.purger.sinaweibo.monitor.Constants.EXTRA_NAME;
import static name.leesah.purger.sinaweibo.monitor.report.ProgressReport.EntityType.STATUS;
import static name.leesah.weiboinjector.Constants.APP_KEY;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.text.WordUtils.capitalizeFully;
import static org.parceler.Parcels.wrap;

/**
 * Created by sah on 2017-04-14.
 */

public class Injector extends JobService {

    private JobParameters params;
    private Oauth2AccessToken accessToken;
    private RequestQueue requestQueue;
    private LocalBroadcastManager broadcastManager;

    @Override
    public boolean onStartJob(JobParameters params) {
        this.params = params;

        broadcastManager = LocalBroadcastManager.getInstance(this);
        accessToken = readAccessToken(this);
        if (!accessToken.isSessionValid()) {
            accessToken = Oauth2AccessToken.parseAccessToken("{" +
                    "\"uid\" : \"6208583426\",\n" +
                    "\"access_token\" : \"2.003KaKmG0tb1s79e5be93b61Q_gfBC\",\n" +
                    "\"refresh_token\" : \"2.003KaKmG0tb1s7955ffa17c7de4zxB\",\n" +
                    "\"phone_num\" : \"\",\n" +
                    "\"expires_in\" : \"1649698283987\"}");
            AccessTokenKeeper.writeAccessToken(this, accessToken);
        }
        requestQueue = newRequestQueue(this);

        new Handler().post(this::postRandomStatus);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        requestQueue.stop();
        return true;
    }

    private void postRandomStatus() {
        String text = range(0, 16 + new Random().nextInt(16))
                .mapToObj(i -> capitalizeFully(randomAlphabetic(2, 8)))
                .collect(joining(" "));

        Request<JSONObject> request = new WeiboRequest(
                POST, "statuses/update.json",
                APP_KEY, accessToken.getUid(), accessToken.getToken(),
                singletonMap("status", text),
                this::onResponse, this::onError);

        requestQueue.add(request);
    }

    private void onResponse(JSONObject response) {
        try {
            report(new ProgressReport.Destroyed(STATUS, response.getString("text")));
        } catch (JSONException e) {
            report(new ProgressReport.Destroyed(STATUS, response.toString()));
        } finally {
            postRandomStatus();
        }
    }

    private void onError(VolleyError e) {
        if (isNetworkOverheated(e)) {
            jobFinished(params, true);
            info("Stopped for now.");
        } else {
            error(new String(e.networkResponse.data));
            jobFinished(params, false);
            info("Stopped for good.");
        }
    }

    List<String> NETWORK_OVERHEAT_ERROR_CODES = Arrays.asList("10022", "10023", "10024", "20016");

    protected boolean isNetworkOverheated(VolleyError error) {
        try {
            JSONObject jsonObject = new JSONObject(new String(error.networkResponse.data));
            return NETWORK_OVERHEAT_ERROR_CODES.contains(jsonObject.getString("error_code"));
        } catch (JSONException e) {
            return false;
        }
    }

    private void info(String message) {
        report(new ProgressReport.Information(STATUS, message));
    }

    private void error(String message) {
        report(new ProgressReport.Error(STATUS, message));
    }

    private void report(ProgressReport report) {
        Log.d("Injector", format(US, "Reporting:\n%s", report.message));
        broadcastManager.sendBroadcast(
                new Intent(ACTION).putExtra(EXTRA_NAME, wrap(report)));
    }

}

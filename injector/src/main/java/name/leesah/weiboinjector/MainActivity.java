package name.leesah.weiboinjector;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import name.leesah.purger.sinaweibo.monitor.MonitorActivity;

public class MainActivity extends AppCompatActivity {

    private static final long HALF_MINUTE = 1000 * 30;
    private static final long ONE_MINUTE = HALF_MINUTE * 2;
    public static final int JOB_ID = Injector.class.hashCode();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JobScheduler jobScheduler = getSystemService(JobScheduler.class);

        if (jobScheduler.getPendingJob(JOB_ID) == null) {
            JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, new ComponentName(this, Injector.class))
                    .setOverrideDeadline(ONE_MINUTE)
                    .setMinimumLatency(HALF_MINUTE)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
            jobScheduler.schedule(builder.build());
        }

        startActivity(new Intent(this, MonitorActivity.class));
        finish();
    }

}

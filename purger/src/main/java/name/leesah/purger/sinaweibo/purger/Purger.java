package name.leesah.purger.sinaweibo.purger;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import name.leesah.purger.sinaweibo.R;
import name.leesah.purger.sinaweibo.monitor.report.ProgressReport;
import name.leesah.sinaweiboapi.DestroyingRequest;
import name.leesah.sinaweiboapi.ListingRequest;

import static com.android.volley.toolbox.Volley.newRequestQueue;
import static name.leesah.purger.sinaweibo.Constants.APP_KEY;
import static name.leesah.purger.sinaweibo.monitor.Constants.ACTION;
import static name.leesah.purger.sinaweibo.monitor.Constants.EXTRA_NAME;
import static org.parceler.Parcels.wrap;

/**
 * Created by sah on 2017-04-13.
 */

public abstract class Purger extends JobService {
    static final String EXTRA_UID = "uid";
    static final String EXTRA_TOKEN = "token";
    private RequestQueue requestQueue;
    private JobParameters params;
    private String uid;
    private String token;
    private LocalBroadcastManager broadcastManager;

    @Override
    public boolean onStartJob(JobParameters params) {
        this.params = params;
        PersistableBundle extras = params.getExtras();
        uid = extras.getString(EXTRA_UID);
        token = extras.getString(EXTRA_TOKEN);

        broadcastManager = LocalBroadcastManager.getInstance(this);

        requestQueue = newRequestQueue(this);
        new Handler().post(this::listItems);
        info(getString(R.string.purge_started));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        stopRequestQueue();
        info(getString(R.string.purge_stopped));
        return true;
    }

    void listItems() {
        ListingRequest request = buildListingRequest(APP_KEY, uid, token, this::onListingResponse, this::onListingError);
        requestQueue.add(request);
    }

    private void destroyItems(List<String> ids) {
        for (String id : ids) {
            DestroyingRequest request = buildDestroyingRequest(APP_KEY, uid, token, id, this::onDestroyingResponse, this::onDestroyingError);
            requestQueue.add(request);
        }
    }

    public void onListingResponse(JSONObject response) {
        try {
            List<String> ids = getIdsFromListingResponse(response);
            if (ids.isEmpty())
                onPurgeComplete();
            else {
                info(getResources().getQuantityString(R.plurals.n_items_to_destroy, ids.size()));
                destroyItems(ids);
                listItems();
            }
        } catch (JSONException e) {
            error(e.getMessage());
            throw new IllegalStateException(e);
        }
    }

    void onDestroyingResponse(JSONObject response) {
        try {
            report(new ProgressReport.Destroyed(getEntityType(), response.getString("text")));
        } catch (JSONException e) {
            report(new ProgressReport.Destroyed(getEntityType(), response.toString()));
            throw new IllegalStateException(e);
        }
    }

    public void onListingError(VolleyError error) {
        onAnyError(error);
        if (!isNetworkOverheated(error)) stopAndWaitForReschedule();
    }

    public void onDestroyingError(VolleyError error) {
        onAnyError(error);
    }

    private void onAnyError(VolleyError error) {
        if (isNetworkOverheated(error)) onNetworkOverheat();
        else error(new String(error.networkResponse.data));
    }

    void onPurgeComplete() {
        report(new ProgressReport.PurgeComplete(getEntityType()));
        stopRequestQueue();
        jobFinished(params, false);
    }

    private void onNetworkOverheat() {
        report(new ProgressReport.Overheat(getEntityType()));
        stopAndWaitForReschedule();
    }

    void stopAndWaitForReschedule() {
        stopRequestQueue();
        info(getString(R.string.till_next_time));
        jobFinished(params, true);
    }

    List<String> NETWORK_OVERHEAT_ERROR_CODES = Arrays.asList("10022", "10023", "10024");

    protected boolean isNetworkOverheated(VolleyError error) {
        try {
            JSONObject jsonObject = new JSONObject(new String(error.networkResponse.data));
            return NETWORK_OVERHEAT_ERROR_CODES.contains(jsonObject.getString("error_code"));
        } catch (JSONException e) {
            return false;
        }
    }

    void stopRequestQueue() {
        requestQueue.cancelAll(request -> true);
        requestQueue.stop();
        info(getString(R.string.network_activity_stopped));
    }


    private static final Class[] PURGE_JOB_CLASSES = new Class[]{
            StatusPurger.class,
            FriendshipPurger.class,
            CommentPurger.class};

    public static void scheduleJobs(Context context, Oauth2AccessToken accessToken) {
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);

        for (Class clazz : PURGE_JOB_CLASSES)
            jobScheduler.schedule(buildJobInfo(context, accessToken, clazz));
    }

    private static JobInfo buildJobInfo(Context context, Oauth2AccessToken accessToken, Class clazz) {
        PersistableBundle extras = new PersistableBundle();
        extras.putString(EXTRA_UID, accessToken.getUid());
        extras.putString(EXTRA_TOKEN, accessToken.getToken());
        return new JobInfo.Builder(clazz.hashCode(), new ComponentName(context, clazz))
                .setExtras(extras)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setRequiresDeviceIdle(true)
                .setRequiresCharging(false)
                .setOverrideDeadline(1024)
                .setPersisted(true)
                .build();
    }

    private void info(String message) {
        report(new ProgressReport.Information(getEntityType(), message));
    }

    private void error(String message) {
        report(new ProgressReport.Error(getEntityType(), message));
    }

    private void report(ProgressReport report) {
        broadcastManager.sendBroadcast(
                new Intent(ACTION).putExtra(EXTRA_NAME, wrap(report)));
    }

    protected abstract ProgressReport.EntityType getEntityType();

    @NonNull
    protected abstract ListingRequest buildListingRequest(String appkey, String uid, String token, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener);

    protected abstract List<String> getIdsFromListingResponse(JSONObject jsonObject) throws JSONException;

    protected abstract DestroyingRequest buildDestroyingRequest(String appkey, String uid, String token, String id, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener);

    void setRequestQueue(RequestQueue requestQueue) {
        this.requestQueue = requestQueue;
    }

    void setParams(JobParameters params) {
        this.params = params;
    }

    void setBroadcastManager(LocalBroadcastManager broadcastManager) {
        this.broadcastManager = broadcastManager;
    }
}

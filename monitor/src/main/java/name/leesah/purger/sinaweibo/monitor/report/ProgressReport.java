package name.leesah.purger.sinaweibo.monitor.report;

import org.joda.time.LocalDateTime;
import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import static org.joda.time.LocalDateTime.now;

/**
 * Created by sah on 2017-05-14.
 */
public abstract class ProgressReport {

    public enum EntityType {STATUS, COMMENT, FRIENDSHIP}

    public LocalDateTime timestamp;
    public EntityType entityType;
    public String message;

    ProgressReport(EntityType entityType, String message) {
        this.entityType = entityType;
        this.timestamp = now();
        this.message = message;
    }

    @Parcel
    public static class Information extends ProgressReport {
        @ParcelConstructor
        public Information(EntityType entityType, String message) {
            super(entityType, message);
        }
    }

    @Parcel
    public static class Destroyed extends ProgressReport {
        @ParcelConstructor
        public Destroyed(EntityType entityType, String message) {
            super(entityType, message);
        }
    }

    @Parcel
    public static class Error extends ProgressReport {
        @ParcelConstructor
        public Error(EntityType entityType, String message) {
            super(entityType, message);
        }
    }

    @Parcel
    public static class Overheat extends ProgressReport {
        @ParcelConstructor
        public Overheat(EntityType entityType) {
            super(entityType, null);
        }
    }

    @Parcel
    public static class PurgeComplete extends ProgressReport {
        @ParcelConstructor
        public PurgeComplete(EntityType entityType) {
            super(entityType, null);
        }
    }

}

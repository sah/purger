package name.leesah.purger.sinaweibo.monitor;

/**
 * Created by sah on 2017-05-14.
 */

public interface Constants {
    String ACTION = "name.leesah.purger.sinaweibo.monitor:action.report";
    String EXTRA_NAME = "name.leesah.purger.sinaweibo.monitor:extra.report";
}

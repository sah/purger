package name.leesah.purger.sinaweibo.monitor;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import name.leesah.purger.sinaweibo.monitor.report.ProgressReport;

import static java.lang.String.format;
import static java.util.Locale.US;
import static org.joda.time.format.ISODateTimeFormat.dateHourMinuteSecond;

/**
 * Created by sah on 2017-05-14.
 */

public class ProgressReportView extends LinearLayout {

    private final ImageView typeIcon;
    private final TextView typeText;
    private final TextView timestamp;
    private final TextView message;

    public ProgressReportView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.report, this);

        typeIcon = (ImageView) findViewById(R.id.type_icon);
        typeText = (TextView) findViewById(R.id.type_text);
        timestamp = (TextView) findViewById(R.id.timestamp);
        message = (TextView) findViewById(R.id.message);
    }

    public void updateWith(ProgressReport report) {
        timestamp.setText(dateHourMinuteSecond().print(report.timestamp));
        updateEntityTypeIndicators(report);
        updateMessage(report);
    }

    private void updateEntityTypeIndicators(ProgressReport report) {
        switch (report.entityType) {
            case STATUS:
                typeText.setText(R.string.type_status);
                break;
            case COMMENT:
                typeText.setText(R.string.type_comment);
                break;
            case FRIENDSHIP:
                typeText.setText(R.string.type_friendship);
                break;
            default:
                throw new IllegalArgumentException(format(US, "Unexpected entity type: [%s].", report.entityType));
        }
    }

    protected void updateMessage(ProgressReport report) {
        if (message != null && report.message != null)
            message.setText(report.message);
    }

}

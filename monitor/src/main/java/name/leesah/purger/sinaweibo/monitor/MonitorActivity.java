package name.leesah.purger.sinaweibo.monitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import name.leesah.purger.sinaweibo.monitor.report.ProgressReport;

import static java.lang.String.format;
import static java.util.Locale.US;
import static name.leesah.purger.sinaweibo.monitor.Constants.ACTION;
import static name.leesah.purger.sinaweibo.monitor.Constants.EXTRA_NAME;
import static org.parceler.Parcels.unwrap;

public class MonitorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);

        ArrayList<ProgressReport> reports = new ArrayList<>();
        MessageArrayAdapter adapter = new MessageArrayAdapter(this, reports);
        ((ListView)findViewById(android.R.id.list)).setAdapter(adapter);

        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ProgressReport report = unwrap(intent.getParcelableExtra(EXTRA_NAME));
                reports.add(report);
                adapter.notifyDataSetChanged();
                Log.d("Monitor", format(US, "Received:\n%s", report.message));
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(ACTION));
    }

    private class MessageArrayAdapter extends ArrayAdapter<ProgressReport>{

        MessageArrayAdapter(@NonNull Context context, @NonNull List<ProgressReport> reports) {
            super(context, 0, reports);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            ProgressReportView view = convertView instanceof ProgressReportView ?
                    (ProgressReportView) convertView :
                    new ProgressReportView(getContext(), null);

            view.updateWith(getItem(position));
            return view;
        }
    }
}
